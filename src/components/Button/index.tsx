import { Button } from "grommet";
import styled from "styled-components";

export default styled(Button)`
  border-radius: 8px;
  font-size: 12px;
  padding: 8px;
  text-align: center;
`;
